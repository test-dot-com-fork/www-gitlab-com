---
layout: job_page
title: "Developer"
---

## Developers Roles at GitLab

Developers at GitLab work on our product. This includes both the open source version of GitLab, the enterprise editions, and the GitLab.com service as well. They work with peers on teams dedicated to areas of the product. They work together with product managers, designers, and backend or frontend developers to solve common goals.


### Junior Developer

Junior Developers share the same requirements outlined below, but typically join with less or alternate experience in one of the key areas of Developer expertise (Ruby on Rails, Go, Git, reviewing code).


### Developer

* Write good code
* Peer review others’ code
* Ship small features independently
* Be positive and solution oriented
* Good written and verbal communication skills
* Constantly improve product quality, security, and performance


### Senior Developer

The Senior Developer role extends the [Developer](#developer) role.

* Write _great_ code
* Ship _medium_ features independently
* Generate architecture recommendations
* _Great_ written and verbal communication skills
* Code screen applicants during hiring process

***

A Senior Developer may want to pursue the [engineering management track](/jobs/engineering-management) at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

***


### Staff Developer

The Staff Developer role extends the [Senior Developer](#senior-developer) role.

* Write _exquisite_ code
* Ship _large_ features independently
* Make architecture decisions
* Implement technical and process improvements
* _Exquisite_ written and verbal communication skills
* Author technical architecture documents for epics
* Author code tests for hiring process and screen applicants
* Write public blog posts


### Distinguished Developer

The Distinguished Developer role extends the [Staff Developer](#staff-developer) role.

* Ship _large_ feature sets with team
* _Generate_ technical and process improvements
* Contribute to the sense of psychological safety on your team
* Work cross-departmentally
* Be a technical mentor for developers
* Author architecture documents for epics
* Hold team members accountable within their roles


### Engineering Fellow

The Engineering Fellow role extends the [Distinguished Developer](#distinguished-developer) role.

* Work on our hardest technical problems
* Ship _extra-large_ feature sets with team
* Help create the sense of psychological safety in the department
* Represent the company publicly at conferences
* Work directly with customers
