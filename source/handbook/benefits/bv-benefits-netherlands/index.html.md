---
layout: markdown_page
title: "GitLab BV (Netherlands) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Specific to Netherlands based employees

Dutch employees get the customary month of vacation money in the month of May.
